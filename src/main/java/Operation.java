

import javax.swing.*;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Amacus5 on 09.03.2017.
 */
public class Operation {

    public Polynomial getPolynomial(String p1) {// desface polinomul introdus ca si text pana la coeficient si exponent si creaza lista de polinoame
        Polynomial poly = new Polynomial();
        p1 = p1.replace("-", "+-");
        String[] arr = p1.split("\\+");
        for (String split : arr) {
            String[] arr1 = split.split("x\\^");
            try {
            Monomial mon = new Monomial(0.0, 0);
            if (arr1.length == 2) {//ax^b
                if (arr1[0].equals("")) {//x^b
                    mon.setCoeff(1.0);
                    mon.setExponent(Integer.valueOf(arr1[1]));
                    poly.addMon(mon);
                } else if (arr1[0].equals("-")) {//-x^b
                    mon.setCoeff(-1.0);
                    mon.setExponent(Integer.valueOf(arr1[1]));
                    poly.addMon(mon);
                } else {
                    mon.setCoeff(Double.parseDouble(arr1[0]));//ax^b
                    mon.setExponent(Integer.valueOf(arr1[1]));
                    poly.addMon(mon);
                }
            } else{
                if (!(arr1[0].equals(""))) {
                    if (arr1[0].equals("-x")) {//-x
                        mon.setCoeff(-1.0);
                        mon.setExponent(1);
                        poly.addMon(mon);
                    }else if (arr1[0].equals("x")) {//x
                        mon.setCoeff(1.0);
                        mon.setExponent(1);
                        poly.addMon(mon);
                    } else if (arr1[0].contains("-")) {//-a
                        if (split.length() >= 2 && !(split.contains("x"))) {
                            mon.setCoeff(Double.parseDouble(arr1[0]));
                            mon.setExponent(0);
                            poly.addMon(mon);
                        } else {//-ax
                            String[] arr2 = arr1[0].split("x");
                            mon.setCoeff(Double.parseDouble(arr2[0]));
                            mon.setExponent(1);
                            poly.addMon(mon);
                        }
                    } else {//ax
                        if (split.contains("x")) {
                            String[] arr2 = arr1[0].split("x");
                            mon.setCoeff(Double.parseDouble(arr2[0]));
                            mon.setExponent(1);
                            poly.addMon(mon);
                        } else {//a
                            mon.setCoeff(Double.parseDouble(arr1[0]));
                            mon.setExponent(0);
                            poly.addMon(mon);
                        }
                    }

                }//*
            }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Incorrect polynomial !!! Use ^ for power and x for variable");
                return null;
            }

        }
        return poly;
    }


    public void sort(Polynomial p1) {// sorteaza polinomul dupa grade
        Collections.sort(p1.getList(), new Comparator<Monomial>() {
            @Override
            public int compare(Monomial o1, Monomial o2) {
                if (o1.getExponent() == (o2.getExponent()))
                    return 0;
                if (o1.getExponent() < (o2.getExponent()))
                    return 1;
                else {
                    return -1;
                }
            }
        });

    }
    public void removed(Polynomial p1){ // aduna coeficientii monoamelor cu acelasi grad si elimina pe cel care a ramas in plus
        for(int i=0;i<p1.size()-1;i++) {
            if(p1.getList().get(i).getExponent() == p1.getList().get(i+1).getExponent())
            {
                p1.getList().get(i).setCoeff(p1.getList().get(i).getCoeff() + p1.getList().get(i+1).getCoeff());
                p1.getList().remove(i+1);
                i--;
            }
        }
    }

    public int maxP(Polynomial p1, Polynomial p2){ // verifica care lista de monoame e mai lunga

        if(p1.size()>p2.size()) {
        return 0;
    } else if(p1.size() <p2.size()) {
        return 1;
    }else {
        return 0;
    }

}
    public Polynomial sum(Polynomial p1, Polynomial p2){ // calculeaza suma
        Polynomial result = new Polynomial(); // rezultatul adunarii

            for(Monomial m : p1.getList()) {
                boolean ok = true;
                Monomial m3 = new Monomial(0.0, 0);
                for (Monomial m2 : p2.getList()) {
                    if (m.getExponent() == m2.getExponent()) {
                        m3.setCoeff(m.getCoeff() + m2.getCoeff());
                        m3.setExponent(m.getExponent());
                        ok = false;
                        result.addMon(m3);
                    }
                }
                if(ok) {

                    m3.setCoeff(m.getCoeff());
                    m3.setExponent(m.getExponent());
                    result.addMon(m3);

                }
            }
        for(Monomial m2 : p2.getList()) { // adauga in lista monoamele care nu au fost adaugate la prima parcurgere
            boolean ok = true;
            Monomial m3 = new Monomial(0.0, 0);
            for (Monomial m : p1.getList()) {
                if (m.getExponent() == m2.getExponent()) {
                    ok = false;
                }
            }
            if (ok) {
                m3.setCoeff(m2.getCoeff());
                m3.setExponent(m2.getExponent());
                result.addMon(m3);

            }
        }
        removed(result);
        return result;

    }
    public Polynomial sub(Polynomial p1, Polynomial p2){ // scaderea polinoamelor (ca la adunare)
        Polynomial result = new Polynomial();
        for(Monomial m : p1.getList()) {
            boolean ok = true;
            Monomial m3 = new Monomial(0.0, 0);
            for (Monomial m2 : p2.getList()) {
                if (m.getExponent() == m2.getExponent()) {
                    m3.setCoeff(m.getCoeff() - m2.getCoeff());
                    m3.setExponent(m.getExponent());
                    ok = false;
                    result.addMon(m3);
                }
            }
            if(ok) {

                m3.setCoeff(m.getCoeff());
                m3.setExponent(m.getExponent());
                result.addMon(m3);

            }
        }
        for(Monomial m2 : p2.getList()) {
            boolean ok = true;
            Monomial m3 = new Monomial(0.0, 0);
            for (Monomial m : p1.getList()) {
                if (m.getExponent() == m2.getExponent()) {
                    ok = false;
                }
            }
            if (ok) {
                m3.setCoeff(-m2.getCoeff());
                m3.setExponent(m2.getExponent());
                result.addMon(m3);

            }
        }
        removed(result);
        return result;

    }
    public Polynomial prod(Polynomial p1, Polynomial p2) {// produs
        Polynomial result = new Polynomial();
        for(Monomial m : p1.getList()) {
            for (Monomial m2 : p2.getList()) {
                Monomial m3 = new Monomial(0.0, 0);
                m3.setCoeff(m.getCoeff()*m2.getCoeff());
                m3.setExponent(m.getExponent()+m2.getExponent());
                result.addMon(m3);
            }
        }
        sort(result);//sortez dupa grade
        removed(result);// sterg monoamele cu acelasi grad
        return result;
    }

    public Polynomial deriv (Polynomial p1){// derivare
        Polynomial result = new Polynomial();
        for(Monomial m: p1.getList()){
            Monomial m1 = new Monomial(0.0,0);
            m1.setCoeff(m.getCoeff()*m.getExponent());
            if(m.getExponent() >0)
            m1.setExponent(m.getExponent()-1);
            result.addMon(m1);
        }
        return result;
    }
    public Polynomial integrate(Polynomial p1){// integrare
        Polynomial result = new Polynomial();
        for(Monomial m: p1.getList()){
            Monomial m1 = new Monomial(0.0,0);
            m1.setCoeff(m.getCoeff()/(m.getExponent()+1));
            m1.setExponent(m.getExponent()+1);
            result.addMon(m1);
        }
        return result;
    }
    public Polynomial div(Polynomial p1, Polynomial p2){
        Polynomial result = new Polynomial();

        return result;
    }
    public String print(Polynomial p1){// transform polinomul in care am rezultatul operatiei intr-un string pe care il afisez
        String s1 = new String();// stringul pe care-l afisez
        String aux = new String();
        for(Monomial m : p1.getList())
        {
            if(m.getExponent() >1 && m.getCoeff()>0 && m.getCoeff() !=1)
            {
                aux=String.format("+%.1fx^%d",m.getCoeff(),m.getExponent());
                s1=s1+(aux);
            }
            if(m.getExponent() >1 && m.getCoeff()<0)
            {
                aux=String.format("%.1fx^%d",m.getCoeff(),m.getExponent());
                s1=s1+(aux);
            }
            if(m.getExponent() >1 && m.getCoeff() == 1){
                aux=String.format("+x^%d",m.getExponent());
                s1=s1+(aux);
            }
            if(m.getExponent() >1 && m.getCoeff() == -1){
                aux=String.format("-x^%d",m.getExponent());
                s1=s1+(aux);
            }
            if(m.getExponent() == 1 && m.getCoeff() == -1){
                aux=String.format("-x");
                s1=s1+(aux);
            }
            if(m.getExponent() == 1 && m.getCoeff() == 1){
                aux=String.format("+x");
                s1=s1+(aux);
            }
            if(m.getExponent() == 1 && m.getCoeff()>1)
            {
                aux=String.format("+%.1fx",m.getCoeff());
                s1=s1+(aux);
            }
            if(m.getExponent() == 1 && m.getCoeff()<0)
            {
                aux=String.format("%.1fx",m.getCoeff());
                s1=s1+(aux);
            }
            if(m.getExponent() == 0 && m.getCoeff()>0)
            {
                aux=String.format("+%.1f",m.getCoeff());
                s1 = s1+(aux);
            }
            if(m.getExponent() == 0 && m.getCoeff()<0)
            {
                aux=String.format("%.1f",m.getCoeff());
                s1 = s1+(aux);
            }
        }
        System.out.print(s1);
        return s1;
    }
}

