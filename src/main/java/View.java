
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Amacus5 on 08.03.2017.
 */
public class View {
    public JButton button1;
    public JPanel panel1;
    public JTextField textField1;
    public JTextField textField2;
    public JTextField textField3;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton divideButton;


    public View() {
        button1.addActionListener(new ActionListener() {//click suma
            public void actionPerformed(ActionEvent e) {

                Operation op = new Operation();
                Polynomial poly1 = new Polynomial(); // primu polinom
                Polynomial poly2 = new Polynomial(); // al 2 lea
                Polynomial poly3 = new Polynomial(); // rezultatul
                Polynomial result = new Polynomial();
                Monomial mon = new Monomial(0.0, 0);
                String p1;
                String p2;
                p1 = new String();
                p2 = new String();
                p1 = textField1.getText();
                p2 = textField2.getText();
                poly1 = op.getPolynomial(p1);
                poly2 = op.getPolynomial(p2);
                op.sort(poly1);//sortez
                op.sort(poly2);
                op.removed(poly1);
                op.removed(poly2);// elimin dublicate
                poly3 = op.sum(poly1, poly2); // operatia
                textField3.setText(op.print(poly3));

                for (Monomial m : poly3.getList()) {
                    System.out.println(m.getCoeff() + " " + m.getExponent());
                }
            }

        });

        button2.addActionListener(new ActionListener() {// click scadere
            public void actionPerformed(ActionEvent e) {

                Operation op = new Operation();
                Polynomial poly1 = new Polynomial();
                Polynomial poly2 = new Polynomial();
                Polynomial poly3 = new Polynomial();
                Polynomial result = new Polynomial();
                Monomial mon = new Monomial(0.0, 0);
                String p1;
                String p2;
                p1 = new String();
                p2 = new String();
                p1 = textField1.getText();
                p2 = textField2.getText();
                poly1 = op.getPolynomial(p1);
                poly2 = op.getPolynomial(p2);
                op.sort(poly1);
                op.sort(poly2);
                op.removed(poly1);
                op.removed(poly2);
                poly3 = op.sub(poly1, poly2);
                textField3.setText(op.print(poly3));

                for (Monomial m : poly3.getList()) {
                    System.out.println(m.getCoeff() + " " + m.getExponent());
                }
            }

        });

        button3.addActionListener(new ActionListener() {//click produs
            public void actionPerformed(ActionEvent e) {

                Operation op = new Operation();
                Polynomial poly1 = new Polynomial();
                Polynomial poly2 = new Polynomial();
                Polynomial poly3 = new Polynomial();
                Polynomial result = new Polynomial();
                Monomial mon = new Monomial(0.0, 0);
                String p1;
                String p2;
                p1 = new String();
                p2 = new String();
                p1 = textField1.getText();
                p2 = textField2.getText();
                poly1 = op.getPolynomial(p1);
                poly2 = op.getPolynomial(p2);
                op.sort(poly1);
                op.sort(poly2);
                op.removed(poly1);
                op.removed(poly2);
                poly3 = op.prod(poly1, poly2);
                textField3.setText(op.print(poly3));

                for (Monomial m : poly3.getList()) {
                    System.out.println(m.getCoeff() + " " + m.getExponent());
                }
            }

        });

        button4.addActionListener(new ActionListener() {// click derivare
            public void actionPerformed(ActionEvent e) {

                Operation op = new Operation();
                Polynomial poly1 = new Polynomial();
                Polynomial poly2 = new Polynomial();
                Polynomial poly3 = new Polynomial();
                Polynomial result = new Polynomial();
                Monomial mon = new Monomial(0.0, 0);
                String p1;
                String p2;
                p1 = new String();
                p2 = new String();
                p1 = textField1.getText();
                p2 = textField2.getText();
                poly1 = op.getPolynomial(p1);
                poly2 = op.getPolynomial(p2);
                op.sort(poly1);
                op.sort(poly2);
                op.removed(poly1);
                op.removed(poly2);
                poly3 = op.deriv(poly1);
                textField3.setText(op.print(poly3));

                for (Monomial m : poly3.getList()) {
                    System.out.println(m.getCoeff() + " " + m.getExponent());
                }
            }

        });
        button5.addActionListener(new ActionListener() {// click integrare
            public void actionPerformed(ActionEvent e) {

                Operation op = new Operation();
                Polynomial poly1 = new Polynomial();
                Polynomial poly2 = new Polynomial();
                Polynomial poly3 = new Polynomial();
                Polynomial result = new Polynomial();
                Monomial mon = new Monomial(0.0, 0);
                String p1;
                String p2;
                p1 = new String();
                p2 = new String();
                p1 = textField1.getText();
                p2 = textField2.getText();
                poly1 = op.getPolynomial(p1);
                poly2 = op.getPolynomial(p2);
                op.sort(poly1);
                op.sort(poly2);
                op.removed(poly1);
                op.removed(poly2);
                poly3 = op.integrate(poly1);
                textField3.setText(op.print(poly3));

                for (Monomial m : poly3.getList()) {
                    System.out.println(m.getCoeff() + " " + m.getExponent());
                }
            }

        });

    }

}
