

/**
 * Created by Amacus5 on 08.03.2017.
 */
public class Monomial {
    private double coeff;
    private int exponent;


    public Monomial(double coeff, int exponent) {//constructor
        this.coeff = coeff;
        this.exponent = exponent;
    }
    public void setCoeff(double x){ // dau valoare coeff

        this.coeff = x;
        }
    public double getCoeff(){ // accesez valoarea

        return this.coeff;
    }
    public void setExponent(int x){

        this.exponent = x;
    }
    public int getExponent(){

        return this.exponent;
    }
}
