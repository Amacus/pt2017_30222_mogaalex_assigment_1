

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amacus5 on 08.03.2017.
 */
public class Polynomial  {
    private List<Monomial> polynomial;
    public Polynomial() {

        this.polynomial = new ArrayList<Monomial>();
    }

    public void addMon( Monomial newMonomial ) {

        this.polynomial.add(newMonomial);
    }
    public int size() {

        return this.polynomial.size();
    }
    public List<Monomial> getList() {

        return this.polynomial;
    }
    public String getString() {
        return this.polynomial.toString();
    }





}
